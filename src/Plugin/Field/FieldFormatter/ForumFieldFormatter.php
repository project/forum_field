<?php

namespace Drupal\forum_field\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\forum\Controller\ForumController;

/**
 * Plugin implementation of the 'forum_interface_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "forum_interface_formatter",
 *   label = @Translation("Forum interface"),
 *   field_types = {
 *     "forum"
 *   }
 * )
 */
class ForumFieldFormatter extends FormatterBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Vocabulary storage.
   *
   * @var \Drupal\taxonomy\VocabularyStorageInterface
   */
  protected $vocabularyStorage;

  /**
   * Term storage.
   *
   * @var \Drupal\taxonomy\TermStorageInterface
   */
  protected $termStorage;

  /**
   * The configuration factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Forum controller.
   *
   * @var \Drupal\forum\Controller\ForumController
   */
  protected $forumController;

  /**
   * Constructs a FormatterBase object.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Entity type manager.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The configuration factory.
   * @param \Drupal\forum\Controller\ForumController $forumController
   *   Forum controller.
   */
  public function __construct(
    $plugin_id,
    $plugin_definition,
    FieldDefinitionInterface $field_definition,
    array $settings,
    $label,
    $view_mode,
    array $third_party_settings,
    EntityTypeManagerInterface $entityTypeManager,
    ConfigFactoryInterface $configFactory,
    ForumController $forumController
  ) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);
    $this->entityTypeManager = $entityTypeManager;
    $this->vocabularyStorage = $this->entityTypeManager->getStorage('taxonomy_vocabulary');
    $this->termStorage = $this->entityTypeManager->getStorage('taxonomy_term');
    $this->configFactory = $configFactory;
    $this->forumController = $forumController;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $forumController = ForumController::create($container);
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('entity_type.manager'),
      $container->get('config.factory'),
      $forumController
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      // Implement default settings.
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    return [
      // Implement settings form.
    ] + parent::settingsForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    // Implement settings summary.

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    foreach ($items as $delta => $item) {
      $elements[$delta] = $this->viewValue($item);
    }

    return $elements;
  }

  /**
   * Generate the output appropriate for one field item.
   *
   * @param \Drupal\Core\Field\FieldItemInterface $item
   *   One field item.
   *
   * @return array
   *   The textual output generated.
   */
  protected function viewValue(FieldItemInterface $item) {
    // If no forum exist.
    $values = $item->getValue();
    if (!isset($values['value']) || !$values['value']) {
      // We need create a new forum.
      $forum = $this->getFieldForum($item);
      // And link it to the field.
      $item->setValue($forum->id());
      $item->getEntity()->save();
    }
    else {
      $forum = $this->termStorage->load($values['value']);
    }
    // Render the forum interface.
    return $this->forumController->forumPage($forum);
  }

  /**
   * Get field forum.
   *
   * If the forum not exist, create it.
   *
   * @param \Drupal\Core\Field\FieldItemInterface $item
   *   One field item.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   Forum.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function getFieldForum(FieldItemInterface $item) {
    $forumContainer = $this->getBundleForumContainer();
    $vocabularyId = $this->configFactory->get('forum.settings')->get('vocabulary');
    $instanceName = $item->getEntity()->label();
    $fieldDefinition = $this->fieldDefinition->getLabel();

    $forumName = $this->t('@field in @entity', [
      '@field' => $fieldDefinition,
      '@entity' => $instanceName,
    ])->render();

    $terms = $this->termStorage->loadByProperties([
      'vid' => $vocabularyId,
      'parent' => $forumContainer->id(),
      'name' => $forumName,
    ]);
    if (count($terms) == 0) {
      $newForum = $this->termStorage->create([
        'vid' => $vocabularyId,
        'parent' => $forumContainer->id(),
        'name' => $forumName,
      ]);
      $newForum->forum_container = FALSE;
      $newForum->save();
      $terms[$newForum->id()] = $newForum;
    }
    return current($terms);
  }

  /**
   * Get bundle forum container.
   *
   * If the forum container not exist, create it.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   Forum container.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function getBundleForumContainer() {
    $entityTypeForum = $this->getEntityTypeForumContainer();
    $vocabularyId = $this->configFactory->get('forum.settings')->get('vocabulary');

    $entityTypeId = $this->fieldDefinition->getTargetEntityTypeId();
    $entityTypeDefinition = $this->entityTypeManager->getDefinition($entityTypeId);

    $bundleLabel = $this->entityTypeManager
      ->getStorage($entityTypeDefinition->getBundleEntityType())
      ->load($this->fieldDefinition->getTargetBundle())
      ->label();

    $terms = $this->termStorage->loadByProperties([
      'vid' => $vocabularyId,
      'parent' => $entityTypeForum->id(),
      'name' => $bundleLabel,
    ]);
    if (count($terms) == 0) {
      $newForum = $this->termStorage->create([
        'vid' => $vocabularyId,
        'parent' => $entityTypeForum->id(),
        'name' => $bundleLabel,
      ]);
      $newForum->forum_container = TRUE;
      $newForum->save();
      $terms[$newForum->id()] = $newForum;
    }
    return current($terms);
  }

  /**
   * Get entity type forum container.
   *
   * If the forum container not exist, create it.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   Forum container.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function getEntityTypeForumContainer() {
    $entityTypeId = $this->fieldDefinition->getTargetEntityTypeId();
    $entityTypeDefinition = $this->entityTypeManager->getDefinition($entityTypeId);
    $entityTypeLabel = $entityTypeDefinition->getPluralLabel()->render();

    $vocabularyId = $this->configFactory->get('forum.settings')->get('vocabulary');

    $terms = $this->termStorage->loadByProperties([
      'vid' => $vocabularyId,
      'parent' => 0,
      'name' => $entityTypeLabel,
    ]);
    if (count($terms) == 0) {
      $newForum = $this->termStorage->create([
        'vid' => $vocabularyId,
        'name' => $entityTypeLabel,
      ]);
      $newForum->forum_container = TRUE;
      $newForum->save();
      $terms[$newForum->id()] = $newForum;
    }
    return current($terms);
  }

}
