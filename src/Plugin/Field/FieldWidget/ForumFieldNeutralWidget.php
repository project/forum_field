<?php

namespace Drupal\forum_field\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'forum_neutral_widget' widget.
 *
 * @FieldWidget(
 *   id = "forum_neutral_widget",
 *   module = "forum_field",
 *   label = @Translation("Forum neutral widget"),
 *   field_types = {
 *     "forum"
 *   }
 * )
 */
class ForumFieldNeutralWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = [];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [$this->t('This field must be visible to init the forum while saving the entity.')];

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    return $element;
  }

}
